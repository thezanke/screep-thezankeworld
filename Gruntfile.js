module.exports = function(grunt) {
  grunt.initConfig({
    email: process.env.SCREEPS_EMAIL,
    password: process.env.SCREEPS_PASS,
    concat: {
      js: {
        src: [
          'setup.js',
          'modules/**/*.js',
          'main.js'
        ],
        dest: 'dist/main.js',
        options: {
          banner: "'use strict';\n",
        }
      }
    },
    screeps: {
      options: {
        email: '<%= email %>',
        password: '<%= password %>',
        branch: 'develop'
      },
      dist: {
        src: ['dist/*.js']
      }
    },
    watch: {
      files: [
        '*.js',
        '**/*.js'
      ],
      tasks: ['concat', 'screeps']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-screeps');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['concat', 'screeps', 'watch']);
};
