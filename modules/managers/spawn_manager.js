let SpawnManager = function () {
  if (!this.spawning && this.room.memory.spawnQueue && this.room.memory.spawnQueue.length > 0) {
    let spawnData = this.room.nextInQueue();
    let role = Roles[spawnData.role];
    let body = role.body(this.room.energyCapacityAvailable);
    // console.log(body);

    let attempt = this.createCreep(body, null, spawnData);

    if (typeof attempt === 'string') {
      let name = attempt;
      let spawnData = this.room.nextInQueue(true);
      // console.log('Spawning "' + name +'" as a ' + spawnData.role);
      if (typeof role.setup === 'function') {
        role.setup.call(this, name);
      }
    }
  }
};
