let CreepManager = function () {
  let role = Roles[this.memory.role];
  if (ROLECALL) this.say(this.memory.role);
  if (!this.compelled) role.work.call(this);
};
