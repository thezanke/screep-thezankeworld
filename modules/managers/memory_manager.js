let MemoryManager = function () {
  let living = Object.keys(Game.creeps);

  // Clear out old creeps from memory
  Object.keys(Memory.creeps).forEach((name) => {
    if (!_.includes(living, name)) delete Memory.creeps[name];
  });

  //// SOURCES
  // Add sources object if doesn't exist
  if (Memory.sources === undefined) Memory.sources = {};

  // Clear old creeps out old room memory
  Object.keys(Memory.rooms).forEach((name) => {
    let roomMemory = Memory.rooms[name];

    // harvesters
    if (roomMemory.harvesters === undefined) {
      roomMemory.harvesters = [];
    }
    roomMemory.harvesters.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.harvesters.splice(i, 1);
      }
    });

    // librarians
    if (roomMemory.librarians === undefined) {
      roomMemory.librarians = [];
    }
    roomMemory.librarians.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.librarians.splice(i, 1);
      }
    });

    // conscripts
    if (roomMemory.conscripts === undefined) {
      roomMemory.conscripts = [];
    }

    roomMemory.conscripts.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.conscripts.splice(i, 1);
      }
    });

    // builders
    if (roomMemory.builders === undefined) {
      roomMemory.builders = [];
    }

    roomMemory.builders.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.builders.splice(i, 1);
      }
    });

    // repairmen
    if (roomMemory.repairmen === undefined) {
      roomMemory.repairmen = [];
    }

    roomMemory.repairmen.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.repairmen.splice(i, 1);
      }
    });
  });
};
