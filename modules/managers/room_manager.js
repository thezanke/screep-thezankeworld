let RoomManager = function () {
  if (this.memory.spawnQueue === undefined) {
    this.memory.spawnQueue = [];
  }

  // HARVESTER
  let totalRoomHarvesters = TOTAL_HARVESTERS_PER_SOURCE * this.sources().length;
  let harvesterTest = this.memory.harvesters.length + _.filter(this.memory.spawnQueue, {
    role: 'harvester'
  }).length < totalRoomHarvesters;

  if (harvesterTest) {
    _.times(totalRoomHarvesters - this.memory.harvesters.length, () => {
      this.spawnCreep({
        role: 'harvester',
        from: this.name,
        target: null
      });
    }, true);
  }

  if (this.storage) {
    let librarianTest = this.memory.librarians.length + _.filter(this.memory.spawnQueue, {
      role: 'librarian'
    }).length < TOTAL_LIBRARIANS_PER_ROOM;

    // LIBRARIAN
    if (librarianTest) {
      this.spawnCreep({
        role: 'librarian',
        from: this.name
      }, true);
    }
  }

  if (this.memory.spawnQueue.length === 0) {

    // Once we have harvesters

    if (this.memory.harvesters.length > 0) {
      // BUILDERS
      if (this.buildables().length > 0 && this.memory.builders.length < TOTAL_BUILDERS_PER_ROOM) {
        this.spawnCreep({
          role: 'builder',
          from: this.name
        });
      }

      // REPAIRMEN
      if (this.memory.repairmen.length < TOTAL_REPAIRMEN_PER_ROOM) {
        this.spawnCreep({
          role: 'repairman',
          from: this.name
        });
      }

      // CONSCRIPTS
      if (this.memory.conscripts.length < TOTAL_CONSCRIPTS_PER_ROOM) {
        this.spawnCreep({
          role: 'conscript',
          from: this.name
        });
      }
    }
  }

  // DROP MANAGEMENT
  // Run only if there is dropped energy in the room
  if (this.energy().length > 0) {
    // Creep roles that can retrieve energy
    let creepRoles = [
      'harvester',
      'librarian',
      'conscript',
      'repairman',
      'builder'
    ];

    // Compel workers to pick up energy
    this.energy().forEach((drop) => {
      let target = drop.pos.findClosestByRange(this.creeps((creep) => {
        return !creep.compelled && _.includes(creepRoles, creep.memory.role) && creep.carry.energy < creep.carryCapacity;
      }));

      if (target) {
        target.compelled = true;

        if (target.pickup(drop) === ERR_NOT_IN_RANGE) {
          target.moveTo(drop);
        }
      }
    });
  }
};
