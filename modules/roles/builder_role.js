Roles.builder = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, WORK, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.builders.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else if (this.room.buildables().length) {
      let target = this.pos.findClosestByRange(this.room.buildables());

      if (target) {
        if (this.build(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    } else {
      this.idle();
    }
  }
};
