Roles.librarian = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 600) {
      return [
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [CARRY, CARRY, CARRY, MOVE, MOVE, MOVE];
  },
  setup: function (name) {
    this.room.memory.librarians.push(name);
  },
  work: function () {
    let target = this.pos.findClosestByRange(this.room.refillables());

    if (target && this.notEmpty()) {
      let attempt = this.transferEnergy(target);
      if (attempt === ERR_NOT_IN_RANGE) {
        this.moveTo(target);
      }
    }
    // If not full
    else if (this.notFull()) {
      let target, attempt;

      // If the room has a storage available
      if (this.room.storage) {
        target = this.room.storage;
        attempt = target.transferEnergy(this);
      }
      // Fallback for no storage
      else {
        target = this.pos.findClosestByRange(this.room.activeSources());
        attempt = this.harvest(target);
      }

      if (attempt === ERR_NOT_IN_RANGE) {
        this.moveTo(target);
      }
    } else {
      this.idle();
    }
  }
};
