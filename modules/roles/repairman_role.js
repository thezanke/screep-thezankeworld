Roles.repairman = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, CARRY, CARRY, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.repairmen.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else {
      let target = this.pos.findClosestByRange(this.room.repairables());
      if (target) {
        if (this.repair(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      } else {
        this.idle();
      }
    }
  }
};
