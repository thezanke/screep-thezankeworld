Roles.conscript = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, WORK, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.conscripts.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else {
      if (this.upgradeController(this.room.controller) === ERR_NOT_IN_RANGE) {
        this.moveTo(this.room.controller);
      }
    }
  }
};
