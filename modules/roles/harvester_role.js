Roles.harvester = {
  // Calculated body to send to createCreep based on total energy capacity
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, CARRY, CARRY, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.harvesters.push(name);
  },
  // The instructions for performing the actual role
  work: function () {
    let source = this.pos.findClosestByRange(_.filter(this.room.sources(), (source) => {
      return !source.targets || source.targets < TOTAL_HARVESTERS_PER_SOURCE;
    }));

    if (source) {
      this.isTargeting(source);
    }

    // If creep is full on energy, drop off
    if (this.energyFull() || (this.notEmpty() && source && source.energy === 0)) {
      let target;

      // Prefer storage UNLESS there are no librarians
      if (this.room.memory.librarians.length > 0 &&
        this.room.storage &&
        this.room.storage.store.energy < this.room.storage.storeCapacity
      ) {
        target = this.room.storage;
      }
      // If storage doesn't exist yet, find closest spawn/extension
      else {
        target = this.pos.findClosestByRange(_.filter(
          _.union(this.room.spawns(), this.room.extensions()), (struct) => {
            return struct.energy < struct.energyCapacity;
          }
        ).sort((a, b) => {
          return a.energy - b.energy;
        }));
      }

      if (target) {
        if (this.transferEnergy(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    }
    // If creep is not full on energy, harvest until full
    else if (source && source.energy > 0) {
      if (this.harvest(source) !== OK) this.moveTo(source);
    }
    else {
      this.idle();
    }
  }
};
