Creep.prototype.energyFull = function () {
  return this.carry.energy === this.carryCapacity;
};

Creep.prototype.notEmpty = function () {
  return this.carry.energy !== 0;
};

Creep.prototype.notFull = function () {
  return this.carry.energy !== this.carryCapacity;
};

Creep.prototype.isEmpty = function () {
  return this.carry.energy === 0;
};

Creep.prototype.isTargeting = function (object) {
  if (object.targets === undefined) {
    object.targets = 0;
  }

  object.targets += 1;
};

Creep.prototype.refillEnergy = function () {
  // Room has a storage structure
  if (this.room.storage) {
    let attempt = this.room.storage.transferEnergy(this);
    if (attempt === ERR_NOT_IN_RANGE) {
      this.moveTo(this.room.storage);
    }
  }
  // Room doesn't have a storage structure
  else {
    let target = this.pos.findClosestByRange(
      _.filter(_.union(this.room.spawns(), this.room.extensions()), (struct) => {
        return struct.energy > 0;
      })
    );

    if (target) {
      if (this.room.memory.spawnQueue.length > 0) {
        this.moveTo(target);
      } else {
        let attempt = target.transferEnergy(this);
        if (attempt === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    }
  }
};

Creep.prototype.idle = function () {
  if (this.room.memory.idleBeacon) {
    let beacon = Game.getObjectById(this.room.memory.idleBeacon);
    if (beacon) {
      this.moveTo(beacon);
    } else {
      this.room.memory.idleBeacon = null;
    }
  }
};
