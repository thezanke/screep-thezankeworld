Room.prototype.spawns = function (filter) {
  if (this._spawns === undefined) {
    this._spawns = this.find(FIND_MY_SPAWNS);
  }

  if (filter) return _.filter(this._spawns, filter);
  return this._spawns;
};

Room.prototype.sources = function (filter) {
  if (this._sources === undefined) {
    this._sources = this.find(FIND_SOURCES);
  }

  if (filter) return _.filter(this._sources, filter);
  return this._sources;
};

Room.prototype.activeSources = function (filter) {
  if (this._activeSources === undefined) {
    this._activeSources = this.find(FIND_SOURCES_ACTIVE);
  }

  if (filter) return _.filter(this._activeSources, filter);
  return this._activeSources;
};

Room.prototype.extensions = function () {
  if (this._extensions === undefined) {
    this._extensions = this.find(FIND_MY_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_EXTENSION
      }
    });
  }

  return this._extensions;
};

Room.prototype.energy = function () {
  if (this._energy === undefined) {
    this._energy = this.find(FIND_DROPPED_ENERGY).sort((a, b) => {
      return b.energy - a.energy;
    });
  }

  return this._energy;
};

Room.prototype.creeps = function (filter) {
  if (this._creeps === undefined) {
    this._creeps = this.find(FIND_MY_CREEPS);
  }

  if (filter) return _.filter(this._creeps, filter);
  return this._creeps;
};

Room.prototype.structures = function (filter) {
  if (this._structures === undefined) {
    this._structures = this.find(FIND_MY_STRUCTURES);
  }

  if (filter) return _.filter(this._structures, filter);

  return this._structures;
};

Room.prototype.roads = function () {
  if (this._roads === undefined) {
    this._roads = this.find(FIND_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_ROAD
      }
    });
  }

  return this._roads;
};

Room.prototype.walls = function () {
  if (this._walls === undefined) {
    this._walls = this.find(FIND_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_WALL
      }
    });
  }

  return this._walls;
};

Room.prototype.repairables = function () {
  if (this._repairables === undefined) {
    this._repairables = _.filter(_.union(
      this.structures(),
      this.roads(),
      this.walls()
    ), (target) => {
      let max = target.hitsMax >= 10000 ? 8000 : target.hitsMax * 0.65;
      return target.hits < max;
    });
  }

  return this._repairables;
};

Room.prototype.buildables = function () {
  if (this._buildables === undefined) {
    this._buildables = this.find(FIND_MY_CONSTRUCTION_SITES);
  }

  return this._buildables;
};

Room.prototype.refillables = function () {
  if (this._refillables === undefined) {
    this._refillables = _.filter(this.structures(), (target) => {
      return _.includes(refillableTypes, target.structureType) && target.energy < target.energyCapacity;
    });
  }

  return this._refillables;
};

Room.prototype.spawnCreep = function (spawnData, force) {
  if (force) return this.memory.spawnQueue.unshift(spawnData);
  else return this.memory.spawnQueue.push(spawnData);
};

Room.prototype.nextInQueue = function (remove) {
  if (remove) return this.memory.spawnQueue.shift();
  return _.first(this.memory.spawnQueue);
};
