'use strict';
// Set to true to have creeps report their role
const ROLECALL = false;

const TOTAL_HARVESTERS_PER_SOURCE = 1;
const TOTAL_BUILDERS_PER_ROOM = 1;
const TOTAL_REPAIRMEN_PER_ROOM = 1;
const TOTAL_LIBRARIANS_PER_ROOM = 1;
const TOTAL_CONSCRIPTS_PER_ROOM = 2;

let refillableTypes = [STRUCTURE_EXTENSION, STRUCTURE_SPAWN];
let Roles = {};

let CreepManager = function () {
  let role = Roles[this.memory.role];
  if (ROLECALL) this.say(this.memory.role);
  if (!this.compelled) role.work.call(this);
};

let MemoryManager = function () {
  let living = Object.keys(Game.creeps);

  // Clear out old creeps from memory
  Object.keys(Memory.creeps).forEach((name) => {
    if (!_.includes(living, name)) delete Memory.creeps[name];
  });

  //// SOURCES
  // Add sources object if doesn't exist
  if (Memory.sources === undefined) Memory.sources = {};

  // Clear old creeps out old room memory
  Object.keys(Memory.rooms).forEach((name) => {
    let roomMemory = Memory.rooms[name];

    // harvesters
    if (roomMemory.harvesters === undefined) {
      roomMemory.harvesters = [];
    }
    roomMemory.harvesters.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.harvesters.splice(i, 1);
      }
    });

    // librarians
    if (roomMemory.librarians === undefined) {
      roomMemory.librarians = [];
    }
    roomMemory.librarians.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.librarians.splice(i, 1);
      }
    });

    // conscripts
    if (roomMemory.conscripts === undefined) {
      roomMemory.conscripts = [];
    }

    roomMemory.conscripts.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.conscripts.splice(i, 1);
      }
    });

    // builders
    if (roomMemory.builders === undefined) {
      roomMemory.builders = [];
    }

    roomMemory.builders.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.builders.splice(i, 1);
      }
    });

    // repairmen
    if (roomMemory.repairmen === undefined) {
      roomMemory.repairmen = [];
    }

    roomMemory.repairmen.forEach((name, i) => {
      if (!_.includes(living, name)) {
        roomMemory.repairmen.splice(i, 1);
      }
    });
  });
};

let RoomManager = function () {
  if (this.memory.spawnQueue === undefined) {
    this.memory.spawnQueue = [];
  }

  // HARVESTER
  let totalRoomHarvesters = TOTAL_HARVESTERS_PER_SOURCE * this.sources().length;
  let harvesterTest = this.memory.harvesters.length + _.filter(this.memory.spawnQueue, {
    role: 'harvester'
  }).length < totalRoomHarvesters;

  if (harvesterTest) {
    _.times(totalRoomHarvesters - this.memory.harvesters.length, () => {
      this.spawnCreep({
        role: 'harvester',
        from: this.name,
        target: null
      });
    }, true);
  }

  if (this.storage) {
    let librarianTest = this.memory.librarians.length + _.filter(this.memory.spawnQueue, {
      role: 'librarian'
    }).length < TOTAL_LIBRARIANS_PER_ROOM;

    // LIBRARIAN
    if (librarianTest) {
      this.spawnCreep({
        role: 'librarian',
        from: this.name
      }, true);
    }
  }

  if (this.memory.spawnQueue.length === 0) {

    // Once we have harvesters

    if (this.memory.harvesters.length > 0) {
      // BUILDERS
      if (this.buildables().length > 0 && this.memory.builders.length < TOTAL_BUILDERS_PER_ROOM) {
        this.spawnCreep({
          role: 'builder',
          from: this.name
        });
      }

      // REPAIRMEN
      if (this.memory.repairmen.length < TOTAL_REPAIRMEN_PER_ROOM) {
        this.spawnCreep({
          role: 'repairman',
          from: this.name
        });
      }

      // CONSCRIPTS
      if (this.memory.conscripts.length < TOTAL_CONSCRIPTS_PER_ROOM) {
        this.spawnCreep({
          role: 'conscript',
          from: this.name
        });
      }
    }
  }

  // DROP MANAGEMENT
  // Run only if there is dropped energy in the room
  if (this.energy().length > 0) {
    // Creep roles that can retrieve energy
    let creepRoles = [
      'harvester',
      'librarian',
      'conscript',
      'repairman',
      'builder'
    ];

    // Compel workers to pick up energy
    this.energy().forEach((drop) => {
      let target = drop.pos.findClosestByRange(this.creeps((creep) => {
        return !creep.compelled && _.includes(creepRoles, creep.memory.role) && creep.carry.energy < creep.carryCapacity;
      }));

      if (target) {
        target.compelled = true;

        if (target.pickup(drop) === ERR_NOT_IN_RANGE) {
          target.moveTo(drop);
        }
      }
    });
  }
};

let SpawnManager = function () {
  if (!this.spawning && this.room.memory.spawnQueue && this.room.memory.spawnQueue.length > 0) {
    let spawnData = this.room.nextInQueue();
    let role = Roles[spawnData.role];
    let body = role.body(this.room.energyCapacityAvailable);
    // console.log(body);

    let attempt = this.createCreep(body, null, spawnData);

    if (typeof attempt === 'string') {
      let name = attempt;
      let spawnData = this.room.nextInQueue(true);
      // console.log('Spawning "' + name +'" as a ' + spawnData.role);
      if (typeof role.setup === 'function') {
        role.setup.call(this, name);
      }
    }
  }
};

Creep.prototype.energyFull = function () {
  return this.carry.energy === this.carryCapacity;
};

Creep.prototype.notEmpty = function () {
  return this.carry.energy !== 0;
};

Creep.prototype.notFull = function () {
  return this.carry.energy !== this.carryCapacity;
};

Creep.prototype.isEmpty = function () {
  return this.carry.energy === 0;
};

Creep.prototype.isTargeting = function (object) {
  if (object.targets === undefined) {
    object.targets = 0;
  }

  object.targets += 1;
};

Creep.prototype.refillEnergy = function () {
  // Room has a storage structure
  if (this.room.storage) {
    let attempt = this.room.storage.transferEnergy(this);
    if (attempt === ERR_NOT_IN_RANGE) {
      this.moveTo(this.room.storage);
    }
  }
  // Room doesn't have a storage structure
  else {
    let target = this.pos.findClosestByRange(
      _.filter(_.union(this.room.spawns(), this.room.extensions()), (struct) => {
        return struct.energy > 0;
      })
    );

    if (target) {
      if (this.room.memory.spawnQueue.length > 0) {
        this.moveTo(target);
      } else {
        let attempt = target.transferEnergy(this);
        if (attempt === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    }
  }
};

Creep.prototype.idle = function () {
  if (this.room.memory.idleBeacon) {
    let beacon = Game.getObjectById(this.room.memory.idleBeacon);
    if (beacon) {
      this.moveTo(beacon);
    } else {
      this.room.memory.idleBeacon = null;
    }
  }
};

Room.prototype.spawns = function (filter) {
  if (this._spawns === undefined) {
    this._spawns = this.find(FIND_MY_SPAWNS);
  }

  if (filter) return _.filter(this._spawns, filter);
  return this._spawns;
};

Room.prototype.sources = function (filter) {
  if (this._sources === undefined) {
    this._sources = this.find(FIND_SOURCES);
  }

  if (filter) return _.filter(this._sources, filter);
  return this._sources;
};

Room.prototype.activeSources = function (filter) {
  if (this._activeSources === undefined) {
    this._activeSources = this.find(FIND_SOURCES_ACTIVE);
  }

  if (filter) return _.filter(this._activeSources, filter);
  return this._activeSources;
};

Room.prototype.extensions = function () {
  if (this._extensions === undefined) {
    this._extensions = this.find(FIND_MY_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_EXTENSION
      }
    });
  }

  return this._extensions;
};

Room.prototype.energy = function () {
  if (this._energy === undefined) {
    this._energy = this.find(FIND_DROPPED_ENERGY).sort((a, b) => {
      return b.energy - a.energy;
    });
  }

  return this._energy;
};

Room.prototype.creeps = function (filter) {
  if (this._creeps === undefined) {
    this._creeps = this.find(FIND_MY_CREEPS);
  }

  if (filter) return _.filter(this._creeps, filter);
  return this._creeps;
};

Room.prototype.structures = function (filter) {
  if (this._structures === undefined) {
    this._structures = this.find(FIND_MY_STRUCTURES);
  }

  if (filter) return _.filter(this._structures, filter);

  return this._structures;
};

Room.prototype.roads = function () {
  if (this._roads === undefined) {
    this._roads = this.find(FIND_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_ROAD
      }
    });
  }

  return this._roads;
};

Room.prototype.walls = function () {
  if (this._walls === undefined) {
    this._walls = this.find(FIND_STRUCTURES, {
      filter: {
        structureType: STRUCTURE_WALL
      }
    });
  }

  return this._walls;
};

Room.prototype.repairables = function () {
  if (this._repairables === undefined) {
    this._repairables = _.filter(_.union(
      this.structures(),
      this.roads(),
      this.walls()
    ), (target) => {
      let max = target.hitsMax >= 10000 ? 8000 : target.hitsMax * 0.65;
      return target.hits < max;
    });
  }

  return this._repairables;
};

Room.prototype.buildables = function () {
  if (this._buildables === undefined) {
    this._buildables = this.find(FIND_MY_CONSTRUCTION_SITES);
  }

  return this._buildables;
};

Room.prototype.refillables = function () {
  if (this._refillables === undefined) {
    this._refillables = _.filter(this.structures(), (target) => {
      return _.includes(refillableTypes, target.structureType) && target.energy < target.energyCapacity;
    });
  }

  return this._refillables;
};

Room.prototype.spawnCreep = function (spawnData, force) {
  if (force) return this.memory.spawnQueue.unshift(spawnData);
  else return this.memory.spawnQueue.push(spawnData);
};

Room.prototype.nextInQueue = function (remove) {
  if (remove) return this.memory.spawnQueue.shift();
  return _.first(this.memory.spawnQueue);
};

Roles.builder = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, WORK, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.builders.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else if (this.room.buildables().length) {
      let target = this.pos.findClosestByRange(this.room.buildables());

      if (target) {
        if (this.build(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    } else {
      this.idle();
    }
  }
};

Roles.conscript = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, WORK, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.conscripts.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else {
      if (this.upgradeController(this.room.controller) === ERR_NOT_IN_RANGE) {
        this.moveTo(this.room.controller);
      }
    }
  }
};

Roles.harvester = {
  // Calculated body to send to createCreep based on total energy capacity
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, CARRY, CARRY, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.harvesters.push(name);
  },
  // The instructions for performing the actual role
  work: function () {
    let source = this.pos.findClosestByRange(_.filter(this.room.sources(), (source) => {
      return !source.targets || source.targets < TOTAL_HARVESTERS_PER_SOURCE;
    }));

    if (source) {
      this.isTargeting(source);
    }

    // If creep is full on energy, drop off
    if (this.energyFull() || (this.notEmpty() && source && source.energy === 0)) {
      let target;

      // Prefer storage UNLESS there are no librarians
      if (this.room.memory.librarians.length > 0 &&
        this.room.storage &&
        this.room.storage.store.energy < this.room.storage.storeCapacity
      ) {
        target = this.room.storage;
      }
      // If storage doesn't exist yet, find closest spawn/extension
      else {
        target = this.pos.findClosestByRange(_.filter(
          _.union(this.room.spawns(), this.room.extensions()), (struct) => {
            return struct.energy < struct.energyCapacity;
          }
        ).sort((a, b) => {
          return a.energy - b.energy;
        }));
      }

      if (target) {
        if (this.transferEnergy(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      }
    }
    // If creep is not full on energy, harvest until full
    else if (source && source.energy > 0) {
      if (this.harvest(source) !== OK) this.moveTo(source);
    }
    else {
      this.idle();
    }
  }
};

Roles.librarian = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 600) {
      return [
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [CARRY, CARRY, CARRY, MOVE, MOVE, MOVE];
  },
  setup: function (name) {
    this.room.memory.librarians.push(name);
  },
  work: function () {
    let target = this.pos.findClosestByRange(this.room.refillables());

    if (target && this.notEmpty()) {
      let attempt = this.transferEnergy(target);
      if (attempt === ERR_NOT_IN_RANGE) {
        this.moveTo(target);
      }
    }
    // If not full
    else if (this.notFull()) {
      let target, attempt;

      // If the room has a storage available
      if (this.room.storage) {
        target = this.room.storage;
        attempt = target.transferEnergy(this);
      }
      // Fallback for no storage
      else {
        target = this.pos.findClosestByRange(this.room.activeSources());
        attempt = this.harvest(target);
      }

      if (attempt === ERR_NOT_IN_RANGE) {
        this.moveTo(target);
      }
    } else {
      this.idle();
    }
  }
};


Roles.repairman = {
  body: function (capacity) {
    if (capacity >= 1800) {
      return [
        WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    if (capacity >= 1300) {
      return [
        WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ];
    }

    return [WORK, CARRY, CARRY, CARRY, MOVE];
  },
  setup: function (name) {
    this.room.memory.repairmen.push(name);
  },
  work: function () {
    // Empty on energy
    if (this.isEmpty()) {
      this.refillEnergy();
    }
    // Not empty on energy
    else {
      let target = this.pos.findClosestByRange(this.room.repairables());
      if (target) {
        if (this.repair(target) === ERR_NOT_IN_RANGE) {
          this.moveTo(target);
        }
      } else {
        this.idle();
      }
    }
  }
};

module.exports.loop = function () {
  // Initialize memory and clean out old memory
  MemoryManager();

  // Run the management modules
  Object.keys(Game.spawns).forEach((n) => {
    SpawnManager.call(Game.spawns[n]);
  });

  Object.keys(Game.rooms).forEach((n) => {
    RoomManager.call(Game.rooms[n]);
  });

  Object.keys(Game.creeps).forEach((n) => {
    CreepManager.call(Game.creeps[n]);
  });
};
