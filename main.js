module.exports.loop = function () {
  // Initialize memory and clean out old memory
  MemoryManager();

  // Run the management modules
  Object.keys(Game.spawns).forEach((n) => {
    SpawnManager.call(Game.spawns[n]);
  });

  Object.keys(Game.rooms).forEach((n) => {
    RoomManager.call(Game.rooms[n]);
  });

  Object.keys(Game.creeps).forEach((n) => {
    CreepManager.call(Game.creeps[n]);
  });
};
