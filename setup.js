// Set to true to have creeps report their role
const ROLECALL = false;

const TOTAL_HARVESTERS_PER_SOURCE = 1;
const TOTAL_BUILDERS_PER_ROOM = 1;
const TOTAL_REPAIRMEN_PER_ROOM = 1;
const TOTAL_LIBRARIANS_PER_ROOM = 1;
const TOTAL_CONSCRIPTS_PER_ROOM = 2;

let refillableTypes = [STRUCTURE_EXTENSION, STRUCTURE_SPAWN];
let Roles = {};
